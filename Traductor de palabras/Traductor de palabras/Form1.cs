﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Traductor_de_palabras
{

    /**
     * @author Ismael Burgos
     * @classForm Formulario de interfaz gráfica del traductor de palabras.
     * @date 2019/02/16 
     */

    public partial class Form1 : Form
    {

        // Objetos SQLCommand y SQLDataReader.

        SqlCommand comandoSql;
        SqlDataReader registro;

        // Establecer la conexión con la base de datos.

        SqlConnection conexion = new SqlConnection("server=DESKTOP-OTI3CR8\\SQLEXPRESS ; database=Idioma ; integrated security = true");

        public Form1()
        {
            InitializeComponent();
        }       

        // Botón Buscar en donde se buscará si hay alguna palabra en la base de datos.

        private void btnBuscar_Click(object sender, EventArgs e)
        {

            if (rdoespanolIngles.Checked)
            {
                try
                {

                    string cadena = "select  Espanol, Ingles from EspanolIngles where Espanol='" + txtPalabra.Text + "';";
                    conexion.Open();
                    comandoSql = new SqlCommand(cadena, conexion);
                    registro = comandoSql.ExecuteReader();
                    registro.Read();


                    txtPalabra.Text = registro["Espanol"].ToString();
                    txtSignificado.Text = registro["Ingles"].ToString();
                    conexion.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);


                }
            }

            if (rdoInglesEspanol.Checked)
            {
                try
                {

                    string cadena = "select  Ingles, Espanol from InglesEspanol where Ingles='" + txtPalabra.Text + "';";
                    conexion.Open();
                    comandoSql = new SqlCommand(cadena, conexion);
                    registro = comandoSql.ExecuteReader();
                    registro.Read();


                    txtPalabra.Text= registro["Ingles"].ToString();
                    txtSignificado.Text = registro["Espanol"].ToString();
                    conexion.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message); 


                }
            }


        }

        
        // Botón Registrar en donde se insertará las palabras en la base de datos.

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (rdoespanolIngles.Checked)
            {
                try
                {
                    conexion.Open();

                    string insertaEspanolIngles = "insert into EspanolIngles(Espanol,Ingles)values('" + txtPalabra.Text + "','" + txtSignificado.Text + "')";
                    SqlCommand comandoSql = new SqlCommand(insertaEspanolIngles, conexion);
                    comandoSql.ExecuteNonQuery();

                    MessageBox.Show("Palabra de Español a Inglés registrada correctamente en la base de datos.");
                    conexion.Close();

                }
                catch (Exception es)
                {
                    MessageBox.Show(es.Message); 
                }               
            }


            if (rdoInglesEspanol.Checked)
            {                
                try
                {
                    conexion.Open();

                    string insertaInglesEspanol = "insert into InglesEspanol(Ingles,Espanol)values('" + txtPalabra.Text + "','" + txtSignificado.Text + "')";
                    SqlCommand comandoSql = new SqlCommand(insertaInglesEspanol, conexion);
                    comandoSql.ExecuteNonQuery();

                    MessageBox.Show("Palabra de Inglés a Español registrada correctamente en la base de datos.");
                    conexion.Close();

                }catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);         
                    
                }
            }
        }

        // Botón eliminar en donde se eliminará los registros que estén disponibles en la base de datos.

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (rdoespanolIngles.Checked)
            {
                try
                {
                    conexion.Open();
                    string borrarEspanolIngles = "delete from EspanolIngles where Espanol ='" + txtPalabra.Text + "';"; 
                    SqlCommand comandoSql = new SqlCommand(borrarEspanolIngles, conexion);
                    comandoSql.ExecuteNonQuery();
                    MessageBox.Show("Palabra de Espanol a Inglés eliminada de la base de datos.");
                    conexion.Close();

                } catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            if (rdoInglesEspanol.Checked)
            {
                try
                {
                    conexion.Open();
                    string borrarInglesEspanol = "delete from InglesEspanol where Ingles ='" + txtPalabra.Text + "';";
                    SqlCommand comandoSql = new SqlCommand(borrarInglesEspanol, conexion);
                    comandoSql.ExecuteNonQuery();
                    MessageBox.Show("Palabra de Inglés a Español eliminada de la base de datos.");
                    conexion.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        // Botón limpiar donde se limpiará lo que se haya escrito en la propia interfaz.

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtPalabra.Text = "";
            txtSignificado.Text = "";


        }

        // Botón Salir que establece la propia salida del programa.

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
    }

     
    


    


    

