﻿namespace Traductor_de_palabras
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblSeleccionarIdioma = new System.Windows.Forms.Label();
            this.txtPalabra = new System.Windows.Forms.TextBox();
            this.lblIgual = new System.Windows.Forms.Label();
            this.txtSignificado = new System.Windows.Forms.TextBox();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.lblTextoPrincipal = new System.Windows.Forms.Label();
            this.rdoespanolIngles = new System.Windows.Forms.RadioButton();
            this.rdoInglesEspanol = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(310, 23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(173, 171);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lblSeleccionarIdioma
            // 
            this.lblSeleccionarIdioma.AutoSize = true;
            this.lblSeleccionarIdioma.Location = new System.Drawing.Point(95, 260);
            this.lblSeleccionarIdioma.Name = "lblSeleccionarIdioma";
            this.lblSeleccionarIdioma.Size = new System.Drawing.Size(97, 13);
            this.lblSeleccionarIdioma.TabIndex = 1;
            this.lblSeleccionarIdioma.Text = "Selecciona Idioma:";
            // 
            // txtPalabra
            // 
            this.txtPalabra.Location = new System.Drawing.Point(365, 274);
            this.txtPalabra.Name = "txtPalabra";
            this.txtPalabra.Size = new System.Drawing.Size(118, 20);
            this.txtPalabra.TabIndex = 3;
            // 
            // lblIgual
            // 
            this.lblIgual.AutoSize = true;
            this.lblIgual.Location = new System.Drawing.Point(496, 277);
            this.lblIgual.Name = "lblIgual";
            this.lblIgual.Size = new System.Drawing.Size(13, 13);
            this.lblIgual.TabIndex = 4;
            this.lblIgual.Text = "=";
            // 
            // txtSignificado
            // 
            this.txtSignificado.Location = new System.Drawing.Point(515, 266);
            this.txtSignificado.Multiline = true;
            this.txtSignificado.Name = "txtSignificado";
            this.txtSignificado.Size = new System.Drawing.Size(140, 28);
            this.txtSignificado.TabIndex = 5;
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(434, 359);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 41);
            this.btnSalir.TabIndex = 7;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(310, 359);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 41);
            this.btnLimpiar.TabIndex = 8;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(540, 23);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 46);
            this.btnBuscar.TabIndex = 9;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Location = new System.Drawing.Point(642, 23);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(75, 46);
            this.btnRegistrar.TabIndex = 10;
            this.btnRegistrar.Text = "Registrar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(591, 97);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(75, 49);
            this.btnEliminar.TabIndex = 12;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // lblTextoPrincipal
            // 
            this.lblTextoPrincipal.AutoSize = true;
            this.lblTextoPrincipal.Location = new System.Drawing.Point(50, 68);
            this.lblTextoPrincipal.Name = "lblTextoPrincipal";
            this.lblTextoPrincipal.Size = new System.Drawing.Size(174, 13);
            this.lblTextoPrincipal.TabIndex = 13;
            this.lblTextoPrincipal.Text = "Bienvenido al traductor de palabras";
            this.lblTextoPrincipal.UseWaitCursor = true;
            // 
            // rdoespanolIngles
            // 
            this.rdoespanolIngles.AutoSize = true;
            this.rdoespanolIngles.Location = new System.Drawing.Point(208, 254);
            this.rdoespanolIngles.Name = "rdoespanolIngles";
            this.rdoespanolIngles.Size = new System.Drawing.Size(100, 17);
            this.rdoespanolIngles.TabIndex = 14;
            this.rdoespanolIngles.TabStop = true;
            this.rdoespanolIngles.Text = "Español - Ingles";
            this.rdoespanolIngles.UseVisualStyleBackColor = true;
            // 
            // rdoInglesEspanol
            // 
            this.rdoInglesEspanol.AutoSize = true;
            this.rdoInglesEspanol.Location = new System.Drawing.Point(208, 277);
            this.rdoInglesEspanol.Name = "rdoInglesEspanol";
            this.rdoInglesEspanol.Size = new System.Drawing.Size(100, 17);
            this.rdoInglesEspanol.TabIndex = 15;
            this.rdoInglesEspanol.TabStop = true;
            this.rdoInglesEspanol.Text = "Ingles - Español";
            this.rdoInglesEspanol.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(801, 450);
            this.Controls.Add(this.rdoInglesEspanol);
            this.Controls.Add(this.rdoespanolIngles);
            this.Controls.Add(this.lblTextoPrincipal);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.txtSignificado);
            this.Controls.Add(this.lblIgual);
            this.Controls.Add(this.txtPalabra);
            this.Controls.Add(this.lblSeleccionarIdioma);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Traductor de Palabras";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblSeleccionarIdioma;
        private System.Windows.Forms.TextBox txtPalabra;
        private System.Windows.Forms.Label lblIgual;
        private System.Windows.Forms.TextBox txtSignificado;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Label lblTextoPrincipal;
        private System.Windows.Forms.RadioButton rdoespanolIngles;
        private System.Windows.Forms.RadioButton rdoInglesEspanol;
    }
}

